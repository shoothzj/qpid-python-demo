import sys

from proton import SSLDomain
from proton.handlers import MessagingHandler
from proton.reactor import Container


class AMQPConsumer(MessagingHandler):
    def __init__(self, server_url, target_address, username, password, cert_file, key_file):
        super(AMQPConsumer, self).__init__()
        self.server_url = server_url
        self.target_address = target_address
        self.username = username
        self.password = password
        self.cert_file = cert_file
        self.key_file = key_file

    def on_start(self, event):
        ssl_domain = SSLDomain(mode=SSLDomain.MODE_CLIENT)
        ssl_domain.set_credentials(self.cert_file, self.key_file, None)
        conn = event.container.connect(self.server_url, user=self.username, password=self.password,
                                       ssl_domain=ssl_domain)
        event.container.create_receiver(conn, self.target_address)

    def on_message(self, event):
        print(f"Received message: {event.message.body}")
        event.connection.close()


if __name__ == "__main__":
    server_url = "amqps://localhost:5671"
    target_address = "example_queue"
    username = "your_username"
    password = "your_password"
    cert_file = "path/to/your/certificate.pem"
    key_file = "path/to/your/private_key.pem"

    try:
        Container(AMQPConsumer(server_url, target_address, username, password, cert_file, key_file)).run()
    except KeyboardInterrupt:
        sys.exit(0)
