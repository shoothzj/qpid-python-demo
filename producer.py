import sys
from proton import Message
from proton.handlers import MessagingHandler
from proton.reactor import Container


class AMQPProducer(MessagingHandler):
    def __init__(self, server_url, target_address, message_body):
        super(AMQPProducer, self).__init__()
        self.server_url = server_url
        self.target_address = target_address
        self.message_body = message_body

    def on_start(self, event):
        conn = event.container.connect(self.server_url)
        self.sender = event.container.create_sender(conn, self.target_address)

    def on_sendable(self, event):
        message = Message(body=self.message_body)
        event.sender.send(message)
        print(f"Sent message: {message.body}")
        event.connection.close()


if __name__ == "__main__":
    server_url = "amqp://localhost:5672"
    target_address = "example_queue"
    message_body = "Hello, AMQP 1.0!"

    try:
        Container(AMQPProducer(server_url, target_address, message_body)).run()
    except KeyboardInterrupt:
        sys.exit(0)
