import sys

from proton.handlers import MessagingHandler
from proton.reactor import Container


class AMQPConsumer(MessagingHandler):
    def __init__(self, server_url, target_address):
        super(AMQPConsumer, self).__init__()
        self.server_url = server_url
        self.target_address = target_address

    def on_start(self, event):
        conn = event.container.connect(self.server_url)
        event.container.create_receiver(conn, self.target_address)

    def on_message(self, event):
        print(f"Received message: {event.message.body}")
        event.connection.close()


if __name__ == "__main__":
    server_url = "amqp://localhost:5672"
    target_address = "example_queue"

    try:
        Container(AMQPConsumer(server_url, target_address)).run()
    except KeyboardInterrupt:
        sys.exit(0)
